import React, { Component } from 'react';
import '../css/Regions.css';

export default class Regions extends Component {

    constructor(props) {
        super(props);
        this.props.getRegions(this.props.id);
        this.props.clearJobs();
    }

    linkClick = (e) => {
        e.preventDefault();
        this.props.history.push(e.target.id)
    }

    render() {
        let regions = [];
        let temp = this.props.regions;
        if (this.props.regions)
        {
            temp.sort((regA, regB) => {
                return regA.id - regB.id
            }).forEach((region, index) => {
                regions.push(<div onClick={this.linkClick} key={index} id={"/" + this.props.id + "/" + region.id}> {region.name + "(" + (region.count?region.count:0) + ")"}</div>)
            })
        }

        return (
            <div className="regions">
                {regions}
            </div>
        );
    }
}
