import React, { Component } from 'react';
import '../css/Breadcrumbs.css';

export default class Breadcrumbs extends Component {

    constructor(props) {
        super(props);
        this.props.getRegions();
    }

    linkClick = (to, level) => {
        if (level < 3)
        {
            this.props.clearjob()
        }
        this.props.history.push(to)
    }

    render() {
        let breadCrumbs = [];

        if (this.props.location.pathname !== "/addjob" && this.props.location.pathname !== "/login" && this.props.location.pathname !== "/register" && this.props.location.pathname !== "/profile" && this.props.location.pathname !== "/changepassword" && this.props.location.pathname !== "/changeuserinfo")
        {
            if (this.props.job)
            {
                breadCrumbs.push(<span key={1}><span onClick={() => this.linkClick("/", 0)}>{"home"}</span>{" > "}</span>);
                let category = this.props.categories.find(cat => {return cat.id === this.props.job.category})
                if (category) breadCrumbs.push(<span key={2}><span onClick={() => this.linkClick("/" + this.props.job.category, 1)}>{category.name}</span>{" > "}</span>);
                let region = this.props.regions.find(reg => { return reg.id === this.props.job.region })
                if (region) breadCrumbs.push(<span key={3}><span onClick={() => this.linkClick("/" + this.props.job.category + "/" + this.props.job.region, 2)}>{region.name}</span>{" > "}</span>);
                breadCrumbs.push(<span key={4} onClick={() => this.linkClick("/job/" + this.props.job.id, 3)}>{this.props.job.title}</span>);
            }
            else
            {
                if (this.props.categories.length > 0)
                {
                    if (this.props.category) {
                        breadCrumbs.push(<span key={1}><span onClick={() => this.linkClick("/", 0)}>{"home"}</span>{" > "}</span>);
                        let category = this.props.categories.find(cat => {return cat.id === this.props.category})
                        if (category)
                        {
                            breadCrumbs.push(<span key={2}><span onClick={() => this.linkClick("/" + this.props.category, 1)}>{category.name}</span>{" > "}</span>);
                        }
                    }
                }

                if (this.props.regions.length > 0) {
                    if (this.props.region) {
                        let region = this.props.regions.find(reg => {
                            return reg.id === this.props.region
                        })
                        if (region) breadCrumbs.push(<span  key={3} onClick={() => this.linkClick("/" + this.props.category + "/" + this.props.region, 2)}>{region.name}</span>);
                    }
                }
            }
        }
        return (
            <>
                {breadCrumbs}
            </>
        );
    }
}
