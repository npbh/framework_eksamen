import React, { Component } from 'react';
import '../css/Jobs.css';
import { Link } from 'react-router-dom';

export default class Jobs extends Component {

    constructor(props) {
        super(props);
        this.props.getJobs(this.props.category, this.props.region, this.props.page?this.props.page:1);
    }

    linkClick = (to, page) => {
        this.props.getJobs(this.props.category, this.props.region, page);
        this.props.history.push(to)
    }


    render() {
        let jobs = [];
        let temp = this.props.jobs;
        if (this.props.jobs)
        {
            temp.forEach((job, index) => {
                jobs.push(<div className="job" key={index}>
                    <Link to={"/job/" + job.id}><h3>{job.title}</h3><span>{job.text.substring(0,400)}</span></Link></div>)
            })
        }

        let pagelinks = []
        if (this.props.pages > 1)
        {
            for (let i = 1; i <= this.props.pages; i++)
            {
                pagelinks.push(<span key={i} onClick={() => this.linkClick("/" + this.props.category + "/" + this.props.region + "/" + i, i)}>{i}</span>)
            }
        }

        return (
            <div className="jobs">
                <div className="pagelinks">
                    {pagelinks}
                </div>
                {jobs.length > 0 ? jobs:<span>{"No job postings found"}</span>}
            </div>
        );
    }
}
