import React, { Component } from 'react';
import '../css/Login.css';

export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
        if (!this.props.auth.loggedIn())this.props.history.push(`/login`);;
    }


    handleClick = (e) => {
        e.preventDefault();
    }

    render() {
        return (
            <div className="loginContainer">
                <form className={"loginForm"} onSubmit={this.onChangePasswordClick}>
                    <label className="err" ref={lbl => this.lbl = lbl} />
                    <input ref={oldPassword => this.oldPassword = oldPassword} type="text" className="text-cos" placeholder="Old Password" />
                    <input ref={newPassword1 => this.newPassword1 = newPassword1} type="text" className="text-cos" placeholder="New Password" />
                    <input ref={newPassword2 => this.newPassword2 = newPassword2} type="text" className="text-cos" placeholder="Repeat Password" />
                    <button type="submit" className="btn-cos">Change Password</button>
                </form>
            </div>
        );
    }

    onChangePasswordClick = (e) => {
        e.preventDefault();
        if (this.newPassword1.value !== this.newPassword2.value){
            this.lbl.innerHTML = "Passwords dont match!!";
        }
        else
        {
            this.props.auth.changePassword(this.oldPassword.value, this.newPassword1.value).then(response => {
                if (response.msg) this.lbl.innerHTML = response.msg;
            })
        }
    }
}
