import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, NavLink } from 'react-router-dom';
import '../css/App.css';
import Login from "./Login";
import AuthService from '../../AuthService';
import Register from "./Register";
import Profile from "./Profile";
import ChangePassword from "./ChangePassword";
import ChangeUserInfo from "./ChangeUserInfo";
import Categories from "./Categories";
import Regions from "./Regions";
import Jobs from "./Jobs";
import Job from "./Job";
import AddJob from "./AddJob";
import Breadcrumbs from "./Breadcrumbs";

export default class App extends Component {
  API_URL = '/api';

  constructor(props) {
    super(props);
    this.Auth = new AuthService(`${this.API_URL}/users`, `${this.API_URL}/jobs`);

    this.state = {
      user: {
        username: "Guest",
        email: "",
        jobs: []
      },
      categories: [],
      regions: [],
      job: "",
      jobs: [],
      pages: 1
    };
    this.getCategories();
  }

  logout = () => {
    this.Auth.logout();
    this.changeLoginStatus();
  }

  getCategories = () => {
    this.Auth.getCategories().then(res => {
      this.setState({categories: res})
    })
  }

  getRegions = (id) => {
    this.Auth.getRegions(id).then(res => {
      this.setState({regions: res})
    })
  }

  getJobs = (cat, reg, page) => {
    this.Auth.getJobs(cat, reg, page).then(res => {
      this.setState({jobs: res.jobs, pages: res.pages})
    })
  }


  getJob = (id) => {
    this.Auth.getJob(id).then(res => {
      this.setState({job: res})
    })
  }

  clearJob = () => {
    this.setState( {job: ""})
  }

  clearJobs = () => {
    this.setState({jobs : []})
  }

  componentDidMount() {
    if(this.Auth.loggedIn())if (this.state.username !== "Guest")this.Auth.getUserInfo().then(res => this.setState({user: res}))
  }

  getUserinfo = () => {
      this.Auth.getUserInfo().then(res => this.setState({user: res}))
  }

  render() {
    let profile = this.Auth.loggedIn() ? <NavLink className="menu-right" activeClassName="active" to={"/profile"}>Profile</NavLink>:null
    let login = this.Auth.loggedIn() ? <a className="menu-right" onClick={this.logout}>Logout</a>:<NavLink className="menu-right" activeClassName="active" to={"/login"}>Login</NavLink>
    let register = this.Auth.loggedIn() ? null:<NavLink className="menu-right" activeClassName="active" to={"/register"}>Register</NavLink>
    let addjob = this.Auth.loggedIn() ? <NavLink className="menu-right" activeClassName="active" exact to={"/addjob"}>Add Job</NavLink>:null
    return (
        <Router>
          <div className="App">
            <header className="header">
              <h1>
                Job Portal
              </h1>
            </header>
            <nav className="topnav">
              <div className="menu">
              <NavLink activeClassName="active" exact to={"/"}>Home</NavLink>
                {login}
                {profile}
                {register}
                {addjob}
              </div>
            </nav>
            <nav className="subnav">
              <div className="breadcrumbs">
                <Route path="/:category?/:region?" render={(props) => <Breadcrumbs {...props} categories={this.state.categories} regions={this.state.regions} region={props.match.params.region} category={props.match.params.category} breadcrumbs={this.state.breadcrumbs} job={this.state.job} getRegions={this.getRegions} clearjob={this.clearJob}/>} />
              </div>
            </nav>

            <section>
              <Switch>
                <Route exact path="/addjob" render={(props) => <AddJob {...props} regions={this.state.regions} categories={this.state.categories} getRegions={this.getRegions} auth={this.Auth} />} />
                <Route exact path="/login" render={(props) => <Login {...props} auth={this.Auth} changeLoginStatus={this.changeLoginStatus} />} />
                <Route exact path="/register" render={(props) => <Register {...props} auth={this.Auth} changeLoginStatus={this.changeLoginStatus} />} />
                <Route exact path="/profile" render={(props) => <Profile {...props} user={this.state.user} getUserJobs={this.getUserJobs} auth={this.Auth} />} />
                <Route exact path="/changepassword" render={(props) => <ChangePassword {...props} auth={this.Auth} />} />
                <Route exact path="/changeuserinfo" render={(props) => <ChangeUserInfo {...props} auth={this.Auth} getuserinfo={this.getUserinfo} user={this.state.user} />} />
                <Route exact path="/job/:id" render={(props) => <Job {...props} id={props.match.params.id} job={this.state.job} getJob={this.getJob} />} />
                <Route exact path="/:category/:region/:page?" render={(props) => <Jobs {...props} jobs={this.state.jobs} category={props.match.params.category} region={props.match.params.region} getJobs={this.getJobs} pages={this.state.pages} page={props.match.params.page} />} />
                <Route exact path="/:category" render={(props) => <Regions {...props} id={props.match.params.category} regions={this.state.regions} getRegions={this.getRegions} clearJobs={this.clearJobs} />} />
                <Route exact path="/" render={(props) => <Categories {...props} categories={this.state.categories} getCat={this.getCategories} clearJobs={this.clearJobs} clearjob={this.clearJob} />} />
              </Switch>
            </section>
          </div>
        </Router>
    );
  }

  changeLoginStatus = () => {
    this.Auth.getUserInfo().then(res => this.setState({user: res? res: {username: "Guest", email: ""}}));
  }
}
