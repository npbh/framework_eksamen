import React from "react"
import { render } from "@testing-library/react"
import Regions from "./Regions";

const testfnc = jest.fn();

it('that it renders the 4 test regions', () => {const regions = render(<Regions id={3} regions={jobdata[1]} getRegions={testfnc} clearJobs={testfnc} />);expect(document.querySelector('.regions').childNodes.length).toBe(4);});

it('that it renders categories with correct names and job count', () => {
    const comp = <Regions id={3} regions={jobdata[1]} getRegions={testfnc} clearJobs={testfnc} />
    const {getByText} = render(comp);
    expect(getByText("test reg 1(2)")).toBeInTheDocument();
    expect(getByText("test reg 2(99)")).toBeInTheDocument();
    expect(getByText("test reg 3(5)")).toBeInTheDocument();
    expect(getByText("test reg 4(0)")).toBeInTheDocument();
})

