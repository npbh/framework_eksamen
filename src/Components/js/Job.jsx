import React, { Component } from 'react';
import '../css/Login.css';

export default class Job extends Component {

    constructor(props) {
        super(props);
        this.props.getJob(this.props.id);
    }


    render() {
        let temp = this.props.job;
        let text  = "";
        if (temp.text)
        {
            text = temp.text;
        }
        
        
        return (
            <div className="loginContainer">
                <h1>{this.props.job.title}</h1>
                <p>
                    <b>Date:</b> {this.props.job.date}<br />
                    <b>Company:</b> {this.props.job.user}
                </p>
                    <p>
                        {text.split("\n").map((item, index) => {
                            return (
                            <span key={index}>
                            {item}
                            <br/>
                            </span>
                            )
                        })}

                        {}
                    </p>
            </div>
        );
    }
}
