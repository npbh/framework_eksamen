import React, { Component } from 'react';
import '../css/Categories.css';

export default class Categories extends Component {

    constructor(props) {
        super(props);
        this.props.getCat();
        this.props.clearJobs();
        this.props.clearjob()
    }

    linkClick = (e) => {
        e.preventDefault();
        this.props.history.push(e.target.id)
    }

    render() {
        let categories = [];
        let temp = this.props.categories;
        if (this.props.categories)
        {
            temp.sort((catA, catB) => {
                let catAName = catA.name.toLowerCase();
                let catBName = catB.name.toLowerCase();
                if (catAName < catBName) return -1;
                if (catAName > catBName) return 1;
                return 0
            }).forEach((category, index) => {
                categories.push(<div onClick={this.linkClick} key={category.name} id={"/" + category.id}> {category.name + " (" + category.count + ")"}</div>)
            })
        }

        return (
            <div id="categories">
                {categories}
            </div>
        );
    }
}
