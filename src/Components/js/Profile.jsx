import React, { Component } from 'react';
import {Link } from 'react-router-dom';
import '../css/Profile.css'

export default class Profile extends Component {

    constructor(props) {
        super(props);
        if (!this.props.auth.loggedIn())this.props.history.push(`/login`);
    }


    handleClick = (e) => {
        e.preventDefault();
    }

    render() {
        let jobs = [];

        if (this.props.user.jobs)
        {
            this.props.user.jobs.forEach(job => {
                jobs.push(<li key={job.id}><Link to={'/job/' + job.id}>{job.title}</Link></li>)
            })
        }

        return (
            <div className="profileContainer">
                <span>Username: {this.props.user.username}</span><br />
                <span>Email: {this.props.user.email}</span><br />
                <span>Company name: {this.props.user.companyname}</span><br />
                <Link to="/changepassword">Change Password</Link> - <Link to="/changeuserinfo">Change Userinfo</Link>
                <h2>Jobs</h2>
                <ul>
                    {jobs}
                </ul>
            </div>
        );
    }
}
