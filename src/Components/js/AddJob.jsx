import React, { Component } from 'react';
import '../css/AddJob.css';
export default class AddJob extends Component {

    constructor(props) {
        super(props);
        if (!this.props.auth.loggedIn())this.props.history.push(`/login`);
        this.props.getRegions();
    }


    render() {
        let regions = [];
        let categories = []
        regions.push(<option key={"reg" + 0} value={"0"}>Vælg Region</option>)
        categories.push(<option key={"cat" + 0} value={"0"}>Vælg kategori</option>)
        if (this.props.regions)
        {
            this.props.regions.forEach((region) => {
                regions.push(<option key={"reg" + region.id} value={region.id}>{region.name}</option>)
            })
        }

        if (this.props.categories)
        {
            this.props.categories.forEach((category) => {
                categories.push(<option key={"cat" + category.id} value={category.id}>{category.name}</option>)
            })
        }

        return (
            <div className="AddJobContainer">
                <h1>Add Job Posting</h1>
                <form className={"loginForm"} onSubmit={this.AddJob}>
                    <label className="err" ref={lbl => this.lbl = lbl} />
                    <input ref={title => this.title = title} type="text" className="text-cos" placeholder="Title" />
                    <textarea ref={text => this.text = text} type="text" className="text-cos" placeholder="Text" />
                    <select ref={region => this.region = region}>
                        {regions}
                    </select>
                    <select ref={category => this.category = category}>
                        {categories}
                    </select>
                    <button type="submit">Add Job Posting</button>
                </form>
            </div>
        );
    }

    AddJob = (e) => {
        e.preventDefault();


        if (this.region.value !== "0" && this.category.value !== "0" && this.title.value.trim().length > 0 && this.text.value.trim().length > 0)
        {
            this.props.auth.addjob(this.title.value, this.text.value, this.category.value, this.region.value).then(response => {
                if (response.id)
                {
                    this.props.history.push(`/job/` + response.id)
                }
                else if (response.msg) this.lbl.innerHTML = response.msg;
            })
        }
    }
}
