import React, { Component } from 'react';
import '../css/Login.css';

export default class Register extends Component {

    handleClick = (e) => {
        e.preventDefault();
        this.props.auth.register(this.username.value,this.password.value, this.email.value)
            .then(response => {
                if(response.msg) {
                    this.lbl.innerHTML = response.msg;
                }
            })
    }

    render() {
        return (
            <div className="loginContainer">
                <form className={"loginForm"} onSubmit={this.handleClick}>
                    <label ref={lbl => this.lbl = lbl} />
                    <input ref={username => this.username = username} placeholder={"Username"}/>
                    <input ref={email => this.email = email} placeholder={"Email"}/>
                    <input type="password" ref={password => this.password = password} placeholder={"Password"}/>
                    <button type="submit">Register</button>
                </form>
            </div>
        );
    }
}
