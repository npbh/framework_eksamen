import React, { Component } from 'react';
import '../css/Login.css';

export default class Login extends Component {

    handleClick = (e) => {
        e.preventDefault();
        this.props.auth.login(this.username.value,this.password.value)
            .then(response => {
                if (response.msg === 'User authenticated successfully')
                {
                    this.props.changeLoginStatus();
                    this.props.history.push(`/`)
                }
                else if(response.msg === 'Username or password missing!')
                {
                    this.lbl.innerHTML = "Username or password missing!";
                }
                else
                {
                    this.lbl.innerHTML = response.msg;
                }
            })
    }

    render() {

        return (
            <div className="loginContainer">
                <form className={"loginForm"} onSubmit={this.handleClick}>
                    <label ref={lbl => this.lbl = lbl} />
                    <input className="username" ref={username => this.username = username} placeholder={"Username"}/>
                    <input className="password" type="password" ref={password => this.password = password} placeholder={"Password"}/>
                    <button type="submit">Login</button>
                </form>
            </div>
        );
    }
}
