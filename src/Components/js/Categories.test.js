import React from "react"
import { render } from "@testing-library/react"
import Categories from "./Categories";

const testfnc = jest.fn();

it('that it renders the 4 test categories', () => {const categories = render(<Categories categories={jobdata[0]} getCat={testfnc} clearJobs={testfnc} clearjob={testfnc} />);expect(document.querySelector('#categories').childNodes.length).toBe(4);});

it('that it renders categories with correct names and job count', () => {
    const comp = <Categories categories={jobdata[0]} getCat={testfnc} clearJobs={testfnc} clearjob={testfnc}/>
    const {getByText} = render(comp);
    expect(getByText("test cat 1 (2)")).toBeInTheDocument();
    expect(getByText("test cat 2 (99)")).toBeInTheDocument();
    expect(getByText("test cat 3 (5)")).toBeInTheDocument();
    expect(getByText("test cat 4 (0)")).toBeInTheDocument();
})