import React, { Component } from 'react';
import '../css/Login.css';
export default class ChangeUserInfo extends Component {

    constructor(props) {
        super(props);
        if (!this.props.auth.loggedIn())this.props.history.push(`/login`);
    }


    render() {
        return (
            <div className="loginContainer">
                <form className={"loginForm"} onSubmit={this.onChangeUserInfoClick}>
                    <label className="err" ref={lbl => this.lbl = lbl} />
                    <input ref={email => this.email = email} type="text" className="text-cos" placeholder="Email" defaultValue={this.props.user.email} />
                    <input ref={companyname => this.companyname = companyname} type="text" className="text-cos" placeholder="Companyname" defaultValue={this.props.user.companyname} />
                    <button type="submit">Change userinfo</button>
                </form>
            </div>
        );
    }

    onChangeUserInfoClick = (e) => {
        e.preventDefault();
            this.props.auth.changeUserInfo(this.email.value, this.companyname.value).then(response => {
                if (response.msg) this.lbl.innerHTML = response.msg;
                this.props.getuserinfo()
            })
    }
}
