import React from "react"
import { render, fireEvent } from "@testing-library/react"
import Login from "./Login";


const testfnc = jest.fn();
const auth = {login: jest.fn().mockImplementation(() => Promise.resolve({}))}

it('check that it renders the login button and input fields with correct placeholder text', () => {
    const comp = <Login auth={auth} changeLoginStatus={testfnc} />
    const {getByText} = render(comp);
    expect(document.querySelector('.username').placeholder).toBe("Username")
    expect(document.querySelector('.password').placeholder).toBe("Password")
    expect(getByText("Login")).toBeInTheDocument();
})

it('fire click evnet on login submit button and expect login function to have been called once ', () => {
    const comp = <Login auth={auth} changeLoginStatus={testfnc} />
    const {getByText} = render(comp);
    fireEvent.click(getByText("Login"));
    expect(auth.login).toHaveBeenCalledTimes(1);
});