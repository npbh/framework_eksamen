var jwtDecode = require('jwt-decode');
/**
 * Service class for authenticating users against an API
 * and storing JSON Web Tokens in the browsers LocalStorage.
 */
class AuthService {

    constructor(auth_api_url, job_api_url) {
        this.auth_api_url = auth_api_url;
        this.job_api_url = job_api_url;
        this.fetch = this.fetch.bind(this);
        this.login = this.login.bind(this);
    }

    login(username, password) {
        return this.fetch(this.auth_api_url + "/login", {
            method: 'POST',
            body: JSON.stringify({
                username,
                password
            })
        }).then(res => {
            if (res.token !== undefined)
            this.setToken(res.token);
            return Promise.resolve(res);
        })
    }

    register(username, password, email)
    {
        return this.fetch(this.auth_api_url, {
            method: 'POST',
            body: JSON.stringify({
                username: username,
                password: password,
                email: email
            })
        }).then(res => {
            if (res.msg !== undefined)
            return Promise.resolve(res);
        })
    }

    changePassword(password, newpassword)
    {
        return this.fetch(this.auth_api_url, {
            method: 'PUT',
            body: JSON.stringify({
                password: password,
                newpassword: newpassword
            })
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    changeUserInfo(email, companyname)
    {
        return this.fetch(this.auth_api_url + "/userinfo", {
            method: 'PUT',
            body: JSON.stringify({
                email: email,
                companyname: companyname
            })
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    getUserInfo()
    {
        return this.fetch(this.auth_api_url, {
            method: 'GET'
        }).then(res => {
            if (res.username !== undefined)
                return Promise.resolve(res);
        })
    }

    loggedIn() {
        if (this.getToken() !== null && this.getToken() !== undefined)
        {
            if (jwtDecode(this.getToken()).exp < Date.now() / 1000) {
                this.logout();
                console.log('token expired');
            }
        }

        return (this.getToken() !== undefined && this.getToken() != null);
    }

    setToken(token) {
        localStorage.setItem('token', token)
    }

    getToken() {
        return localStorage.getItem('token')
    }

    logout() {
        localStorage.removeItem('token');
    }

    addjob(title,text, category, region)
    {
        return this.fetch(this.job_api_url + '/job', {
            method: 'POST',
            body: JSON.stringify({
                title: title,
                text: text,
                region: region,
                category: category
            })
        }).then(res => {
            return Promise.resolve(res);
        })
    }

    getCategories() {
        return fetch(this.job_api_url + "/categories", {
            method: 'GET'
        }).then(res => {
            return res.json()
        })
    }

    getRegions(id) {
        return fetch(this.job_api_url + "/regions" + (id? ("/" + id):""), {
            method: 'GET'
        }).then(res => {
            return res.json()
        })
    }

    getJobs(category, region, page) {
        return fetch(this.job_api_url + "/" + category + "/" + region + "/" + page, {
            method: 'GET'
        }).then(res => {
            return res.json()
        })
    }

    getJob(id) {
        return fetch(this.job_api_url + "/job/" + id, {
            method: 'GET'
        }).then(res => {
            return res.json()
        })
    }


    fetch(url, options) {
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
        .then(response => response.json());
    }
}

export default AuthService;
