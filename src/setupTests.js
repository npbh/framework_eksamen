import "jest-dom/extend-expect"
import "@testing-library/react/cleanup-after-each"

global.jobdata = [
    [{name: "test cat 1", id: 1, count: 2}, {name: "test cat 2", id: 2, count: 99}, {name: "test cat 3", id: 3, count: 5}, {name: "test cat 4", id: 4, count: 0}],
    [{name: "test reg 1", id: 1, count: 2}, {name: "test reg 2", id: 2, count: 99}, {name: "test reg 3", id: 3, count: 5}, {name: "test reg 4", id: 4, count: 0}]
];

