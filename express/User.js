module.exports = (mongoose) => {
    const express = require('express');
    const router = express.Router();
    const bcrypt = require('bcrypt');
    const jwt = require('jsonwebtoken');

    //bruger skema
    let userSchema = mongoose.Schema({
        companyname: String,
        username: String,
        password: String,
        email: String
    });

    let jobModel = mongoose.model('job');
    let userModel = mongoose.model('user', userSchema);
    router.get('/', (req,res) => {
        userModel.findOne({ username: req.user.username}, (err, user) => {
            if (err) return console.log(err);
            if (user)
            {
                jobModel.find({user: req.user.username}, (err, result) => {
                    if (err) return console.log(err);
                    let u = {username: user.username, email: user.email,companyname: user.companyname, jobs: []}
                    if (result)
                    {
                        u.jobs = result;
                    }
                    res.json(u);
                });

            }
            else
            {
                res.json({msg: "user not found"})
            }
        });
    });

    //rute til login
    router.post('/login', (req,res) => {
        const username = req.body.username;
        const password = req.body.password;

        if (!username || !password) {
            let msg = "Username or password missing!";
            console.error(msg);
            res.json({msg: msg});
            return;
        }

        //kigger om brugeren eksisterer
        userModel.findOne({ username: username}, (err, user) => {
            if (user) {
                //sammenligner password i db
                bcrypt.compare(password, user.password, (err, result) => {
                    if (result) {

                            const payload = {
                                username: username,
                                email: user.email,
                                admin: false
                            };
                            const token = jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '365d' });

                            res.json({
                                msg: 'User authenticated successfully',
                                token: token
                            });

                    }
                    else res.json({msg: "Password mismatch!"})
                });
            } else {
                res.json({msg: "User not found!"});
            }
        })
    });

    router.post('/', (req, res) => {
        if (!req.body.password && !req.body.username && !req.body.email)
        {
            res.json({msg: "username, password and email missing"})
        }
        else if (!req.body.password && !req.body.email)
        {
            res.json({msg: "password and email missing"})
        }
        else if (!req.body.username && !req.body.email)
        {
            res.json({msg: "username and email missing"})
        }
        else if (!req.body.password && !req.body.username)
        {
            res.json({msg: "username and password missing"})
        }
        else if (!req.body.password)
        {
            res.json({msg: "password missing"})
        }
        else if(!req.body.username)
        {
            res.json({msg: "username missing"})
        }
        else if(!req.body.email)
        {
            res.json({msg: "email missing"})
        }
        else
        {
            let newUser = new userModel();
            userModel.find({username: req.body.username}, (err, result) => {
                if (result.length > 0)
                {
                    res.json({msg: "username allready taken"});
                }
                else
                {
                    bcrypt.hash(req.body.password, 10, function(err, hash) {
                        if (err)
                        {
                            res.json({msg: "Error " + err})
                        }
                        else
                        {
                            newUser.username = req.body.username;
                            newUser.companyname = req.body.username;
                            newUser.password = hash;
                            newUser.email = req.body.email
                            newUser.save();
                            res.json({msg: "User created"});
                        }
                    });
                }

            })

        }});

    router.put('/', (req,res) => {
        let password = req.body.password;
        let newpassword = req.body.newpassword;

        if (password)
        {
            userModel.findOne({ username: req.user.username}, (err, user) => {
                if (user) {
                    bcrypt.compare(password, user.password, (err, result) => {
                        if (result) {
                            if (newpassword)
                            {
                                bcrypt.hash(newpassword, 10, function(err, hash) {
                                    if (err)
                                    {
                                        res.json({msg: "Error " + err})
                                    }
                                    else
                                    {
                                        user.password = hash;
                                        user.save();
                                        res.json({msg: "password updated"});
                                    }
                                });
                            }
                            else
                            {
                                user.save();
                                res.json("userinfo updated");
                            }
                        }
                        else res.status(401).json({msg: "Password mismatch!"})
                    });
                } else {
                    res.status(404).json({msg: "User not found!"});
                }
            })
        }
        else
        {
            res.status(401).json({msg: "password missing"});
        }
    });

    router.put('/userinfo', (req,res) => {
            console.log(req.body)
            let email = req.body.email;
            let companyname = req.body.companyname;
            userModel.findOne({ username: req.user.username}, (err, user) => {
                if (user) {
                    user.email = email;
                    user.companyname = companyname;
                    user.save();
                    res.status(200).json({msg: "userinfo updated"});

                } else {
                    res.status(404).json({msg: "User not found!"});
                }
            })
    });

    return router;
}