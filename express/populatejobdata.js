const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
mongoose.connect('mongodb://dbuser:niels2502@192.168.1.142/framework_niels?authSource=admin', {useNewUrlParser: true});
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log("Connected to Mongodb")
});

let jobSchema = mongoose.Schema({
    id: Number,
    title: String,
    text: String,
    category: String,
    region: String,
    user: String,
    date: String
})

let userSchema = mongoose.Schema({
    companyname: String,
    username: String,
    password: String,
    email: String
});
let jobModel = mongoose.model('job', jobSchema, "jobs");
let userModel = mongoose.model('user', userSchema);

let categories = ["it","ledelse","ingenioer","handel", "industri", "salg", "undervisning", "kontor", "social", "oevrige"]
let regions = ["storkoebenhavn", "nordsjaelland", "region-sjaelland", "fyn", "region-nordjylland", "region-midtjylland", "sydjylland", "bornholm", "skaane", "groenland", "faeroeerne", "udlandet", "danmark"]

let companyNames = [
    "twicee", "cynoid", "nonil", "bovism", "aify", "centijo", "dimia", "monible", "myose", "yakimbee",
    "medizz", "pedism", "outive", "inframm", "quasicy", "corize", "frontible", "cryonte", "bonore",
    "disent", "paleofy", "metayo", "corata", "sumist", "premize", "rezz", "cofix", "feler", "skyescent",
    "poder", "binu", "syity", "pixovee", "wikicee", "democero", "unend", "unizz", "superism", "omnidoo",
    "cogiva", "cedor", "archent", "misile", "midile", "duveo", "fanix", "paranoodle", "genent", "iner",
    "manumbu"
]

let jobtitles = [
    "Receptionist",
    "IT Manager",
    "School Counselor",
    "Medical Secretary",
    "Security Guard",
    "Housekeeper",
    "Zoologist",
    "Musician",
    "Sports Coach",
    "Speech-Language Pathologist",
    "Chef",
    "Compliance Officer",
    "Construction Manager",
    "Software Developer",
    "Art Director",
    "Physicist",
    "Logistician",
    "Janitor",
    "Loan Officer",
    "College Professor",
    "Social Worker",
    "Event Planner",
    "Dentist",
    "Firefighter",
    "Maintenance & Repair Worker",
    "Food Scientist",
    "Physician",
    "Medical Assistant",
    "Patrol Officer",
    "Accountant",
    "Veterinarian",
    "Coach",
    "Recreational Therapist",
    "Landscape Architect",
    "Landscaper & Groundskeeper",
    "Desktop publisher",
    "Teacher Assistant",
    "Cost Estimator",
    "Market Research Analyst",
    "Dancer",
    "Actuary",
    "Farmer",
    "Carpenter",
    "Systems Analyst",
    "Secretary",
    "Police Officer",
    "Microbiologist",
    "Cashier",
    "Physical Therapist",
    "Executive Assistant",
    "Bookkeeping clerk",
    "Respiratory Therapist",
    "Epidemiologist",
    "Architect",
    "Financial Advisor",
    "Marriage & Family Therapist",
    "Writer",
    "Statistician",
    "Painter",
    "Librarian",
    "Paralegal",
    "Economist",
    "Web Developer",
    "Environmental scientist",
    "Automotive mechanic",
    "Marketing Manager",
    "Plumber",
    "Personal Care Aide",
    "Computer Hardware Engineer",
    "Truck Driver",
    "Database administrator",
    "Paramedic",
    "Hairdresser",
    "Massage Therapist",
    "Photographer",
    "Surveyor",
    "Artist",
    "Mathematician",
    "School Psychologist",
    "Preschool Teacher",
    "Interpreter & Translator",
    "Chemist",
    "Computer Systems Analyst",
    "Childcare worker",
    "Actor",
    "Lawyer",
    "Occupational Therapist",
    "Educator",
    "Professional athlete",
    "Customer Service Representative",
    "High School Teacher",
    "Mason",
    "Reporter",
    "Electrical Engineer",
    "Psychologist",
    "Human Resources Assistant",
    "HR Specialist",
    "Fitness Trainer",
    "Computer Systems Administrator",
    "Registered Nurse",
    "Radiologic Technologist",
    "Judge",
    "Anthropologist",
    "Computer Support Specialist",
    "Computer Programmer",
    "Designer",
    "Clinical Laboratory Technician",
    "Referee",
    "Historian",
    "Insurance Agent",
    "Elementary School Teacher",
    "Public Relations Specialist",
    "Court Reporter",
    "Bus Driver",
    "Budget analyst",
    "Urban Planner",
    "Middle School Teacher",
    "Substance Abuse Counselor",
    "Mechanical Engineer",
    "Electrician",
    "Recreation & Fitness Worker",
    "Diagnostic Medical Sonographer",
    "Editor",
    "Drafter",
    "Pharmacist",
    "Auto Mechanic",
    "Real Estate Agent",
    "Civil Engineer",
    "Telemarketer",
    "Dental Hygienist"
]

let companytypes = ["A/S", "I/S", "Aps"];

let texts = [
    "Lorem ipsum dolor sit amet, sed rutrum amet ornare odio mollis consequatur, tincidunt donec leo suspendisse sem, in netus lorem ipsum nec maecenas. Vestibulum accumsan tortor aliquam tempor, imperdiet voluptatum ut ac eget, risus vel diam lorem, consectetuer viverra rhoncus ante hac nec diam. Imperdiet nam quidem. Pede eu, rutrum libero id vivamus odio. Beatae proin vel, integer augue elit mauris non, donec vitae malesuada elit. Sodales sapien, ac vel, est non dolor eleifend. Lacus purus ipsum lectus, sed augue. Amet consectetuer et vivamus in maecenas a, vitae euismod nostra. Nibh ac ligula consectetuer magni in, dui nunc in sed duis accumsan dolor, dictumst aliquam vivamus tristique netus, condimentum viverra ut.",
    "Justo fringilla, sodales sit venenatis consectetuer fames dictum, cursus ad donec mauris sed. Commodo nunc amet dictum donec in orci, nulla ipsum sem, sit tellus diam fringilla aenean. Ipsum metus habitasse fringilla eu. Pharetra at nisl nec lorem augue, sit vestibulum commodo urna viverra nec platea. Ipsum odio at sollicitudin turpis enim, vivamus sed nullam, etiam proin ipsum sit, nulla sit vivamus diam pharetra hac fringilla, fringilla neque. Suspendisse erat quis, felis ut quam diam venenatis, sociis aliquam libero justo et semper, nonummy sit sem dis.",
    "Voluptatibus dolor, dui scelerisque vulputate tortor metus condimentum varius, porta duis aliquam dui at velit. Nec pulvinar ultrices tellus donec, morbi lorem, et sed sapien nullam velit vestibulum metus, turpis dis ipsum posuere lorem, nec morbi id in. Quis in lacus, mauris erat justo et facilisis wisi sapien. Nulla nulla non nunc varius massa, felis taciti magna eleifend arcu eros, donec vel non urna ipsum egestas donec. Penatibus facilisi amet, odio commodo congue. Justo volutpat, molestie non dignissim, adipiscing amet suscipit rutrum nunc, feugiat pede at dolor. Aenean lectus sed fringilla lobortis, maecenas ac deserunt porttitor donec mi, sit velit mollis, vitae aenean vitae litora mattis. Libero et suspendisse deserunt varius alias. Vitae justo non morbi mi aut cum, commodo ligula metus, felis eget praesent. Commodo tincidunt wisi eget. Metus accumsan lorem placerat, in elit massa urna purus, quisque fringilla cras ultrices pede.",
    "Rhoncus maecenas urna accumsan ea per dolor, ac felis, in iaculis, elementum erat in ut malesuada, quis parturient dolor augue. Faucibus est vel, vel phasellus cras in in, molestie purus vulputate. Eros accumsan dapibus nam mauris etiam ut, class leo taciti est, amet neque semper condimentum est nisl, similique do quis ultrices ac leo aliquam. Felis vivamus rhoncus sollicitudin nulla architecto amet. Ac malesuada, dapibus aut sed, pede velit, et ad pellentesque mi, sed cursus justo dolor porttitor in. Neque justo diam mattis id, nec turpis sagittis dapibus. Vulputate lobortis eu, nunc leo torquent, maecenas diam litora nonummy tempor dui quisque. Quis amet wisi ac, eleifend a eu arcu, a nisl accumsan porttitor. Dolor integer mauris nullam, lectus tortor malesuada odio laoreet, augue nullam mauris wisi vitae fusce, enim quam libero duis in donec duis, sit gravida id egestas proin mauris. Aenean congue leo sunt laoreet, vestibulum amet nulla. In integer vitae nulla rutrum id, faucibus est ut magnis eleifend vivamus rutrum. Integer vel magna velit amet, litora leo, morbi elit justo ad morbi.",
    "Massa massa vestibulum, quis nibh tristique orci aliquet, pulvinar consectetuer et purus orci non, sodales id. Est pretium ante suspendisse, lorem ullamcorper malesuada, sed nec, dignissim et diam, erat suspendisse natoque velit quisque. Id posuere sodales nullam, suspendisse volutpat. Dui diam rutrum at, luctus faucibus eget tristique a etiam molestie, dui tempor adipiscing placerat. Sed convallis etiam neque massa mattis, in ut accumsan posuere in, in sed nunc mi, integer vitae dapibus, amet sit mauris. Euismod et quam ipsum, nam pharetra. A vehicula aute sollicitudin purus, ut turpis pellentesque. Taciti vulputate vulputate sagittis condimentum eu, vel felis magna consectetuer. Vestibulum magna lorem hac a dictum. Placerat aenean accumsan sit, mollis libero tempus sollicitudin sociis sem, amet aliquam libero auctor morbi ac morbi. Urna dui cumque montes vitae diam, vitae ut, suspendisse phasellus lorem donec arcu volutpat rhoncus, blandit erat lobortis id id pellentesque convallis. Aliquam non, eros non amet consequatur vestibulum et, in consectetuer suspendisse in ultrices velit, dolor vulputate, nibh nisl luctus vestibulum.",
    "Vestibulum metus viverra, purus vestibulum. Elementum tellus arcu vel lacus dolor. Vivamus arcu arcu eu, cras duis, faucibus ut eget odio ultrices posuere class, blandit mauris, tincidunt at. Quis quis pede adipiscing, ac lectus a sapien ultricies in augue, tristique mauris nisl viverra id, ante consectetuer, sagittis mollis sit aliquam. Platea ipsum vivamus eu, vitae pellentesque euismod sed accumsan lacinia arcu, mi cras sem sed sodales libero, praesent pretium ut ac, placerat vehicula nulla sed aliquam sit. Suspendisse ultrices nibh turpis nostrud. Porttitor id a sed a ut, nec pulvinar vitae maecenas dolor ut, suscipit dolor amet. Scelerisque ipsum vehicula per posuere nam, a vel in convallis amet, ante placeat eu urna odio, nulla aliquam aliquet curabitur nulla sollicitudin blandit.",
    "Et vel, nunc molestie ante, cursus ac ligula pellentesque gravida, phasellus dolor pede integer in. Netus et mus aut at. Nec condimentum proin nec convallis sed est, leo dolor dapibus ut sed pellentesque nec, dignissim eget purus et enim aliquam libero, a incididunt consequat metus, justo ornare parturient nibh. Duis vel felis lacus iure, non vivamus et wisi vivamus, magna ante magna diam nisl, massa eget porttitor ac. Eget sociis imperdiet, sed amet egestas, sed placerat. Semper erat, phasellus vestibulum magna sollicitudin turpis morbi luctus, erat platea phasellus luctus, sed nunc, elit sed urna elit pellentesque felis nulla. Dolor blandit luctus, mauris porta, pellentesque sed dolor wisi ante dolor quisque. Ullamcorper pede quam ligula consectetuer, pellentesque id at malesuada magna. Arcu dui turpis amet felis nonummy, condimentum justo, quisque volutpat ac ultrices temporibus, et nulla dolor scelerisque tristique tristique. Nonummy ligula risus, nam et veniam id massa donec voluptate, sit vulputate torquent at vel sed, nulla urna fermentum quos integer eget, a eget orci.",
    "Nam sit hendrerit sed aliquam varius. Maecenas molestie diam, faucibus nibh scelerisque, cras nec at et in fusce. Est condimentum nisl auctor turpis pellentesque, fringilla magna, a ligula nec pellentesque lacus sit. Lobortis nulla nulla cras a quis, maecenas elit ultrices justo, wisi velit libero egestas, justo aliquet, quis id risus. Nulla leo voluptate. Odio in tempor eu facilisis, lectus vel sodales at consequat, quisque accumsan, lacinia wisi massa laoreet vestibulum. Tristique faucibus nulla semper, ultrices laoreet quisque, justo fusce ac sollicitudin nascetur risus. Eget bibendum vivamus, diam sodales conubia urna non et, est nonummy natoque eget quisque non, adipiscing vitae quisque sit quis. Gravida nam, ipsum diam nulla mus. Et lobortis venenatis vivamus rutrum nullam. Ut accusamus suspendisse in.",
    "Nullam pellentesque. A duis lacinia ornare, nibh parturient cras, eget praesent lectus eget dolor tincidunt neque. Wisi erat, non est amet, praesent viverra, non tellus enim, et nunc pede. Etiam non etiam urna vivamus, viverra nunc quam praesent libero, erat sed aliquam mauris eu mi, id commodo aliquam inceptos laoreet, suspendisse aliquam vitae. Lorem nec quis nonummy sed nullam. Quam interdum aliquet eleifend quisque, aenean fringilla porta, in elementum sit mi tristique, fusce id, vivamus magnis sagittis sed sed neque lorem. Ut orci, mus autem quam ligula, quisque quam sed tempus nibh integer, mi hymenaeos ante lectus sit euismod. Morbi nunc nisl. Metus wisi pellentesque justo gravida donec metus, quisque consequat. At orci, adipiscing vestibulum molestie ac, elementum at montes eu, a tortor phasellus lacinia tempor. Ac sapien ut faucibus vitae, eget lacus vestibulum blandit morbi, nonummy integer posuere vivamus magna at, velit enim rutrum iaculis. Ante fusce feugiat maecenas mauris sed pede, quis semper massa venenatis quam conubia pellentesque, amet pulvinar vulputate.",
    "Eleifend sociosqu euismod proin ipsum quis, tortor tempor velit id quam, egestas vel, sunt donec. Ipsum porta iaculis et nullam interdum, tincidunt ullamcorper justo ultrices volutpat pretium, libero id mattis nascetur. Dignissim tempus accumsan, est ut nibh hymenaeos pede, in orci urna, placerat duis amet a tristique. Cum at tincidunt mi, a pulvinar velit non sed nullam quis, a est, velit eros lectus in et ante et. In mauris dapibus, augue orci ipsum nunc in, arcu at curabitur ligula. Rhoncus montes imperdiet suscipit sed amet, maecenas etiam, mollis ut, tortor elementum at nulla erat. A luctus ut placerat sem, lacinia quis odio tempor, wisi purus id nunc interdum dui, faucibus ultricies libero faucibus enim, porttitor quis curabitur. Posuere ipsum, suspendisse quam ad amet ullamcorper. In cum vivamus suspendisse pretium, nec viverra nascetur praesent, dui volutpat irure pellentesque quisque.",
    "Ut ipsum ligula eget eget placerat aliquet, porttitor massa malesuada semper mi urna massa. Dignissim ligula nisl ut. Montes aliquam libero a magna et, donec libero lacus iaculis integer leo vel, ut ut morbi erat. Sed ultrices vivamus ab ipsum. Tortor nisl feugiat vestibulum eu lectus, luctus per sit vel ornare tellus egestas. Sed ut curabitur sapien non, sollicitudin a, velit neque, ut natoque at. Non ligula pede amet risus facilisi, urna purus suscipit, habitasse libero enim id egestas magna conubia, et nulla aliquam donec dictumst, magnis metus ac.",
    "Ullamcorper pretium aliquam quam eget aperiam, aliquam nec non vel, vitae mauris ut, suscipit nulla enim cras per mauris vel. Turpis parturient rutrum, etiam amet faucibus ligula, nam massa nec modi, nunc fermentum senectus ligula egestas, aliquam eu proin massa. Lorem tincidunt magna pede sed sollicitudin. A libero, nulla pede quam ante sodales erat, est vel nec luctus, ac tellus dictumst justo viverra, platea mauris fugiat convallis porttitor tincidunt. Proident molestie velit, amet dictum consectetuer sed quisque aenean consectetuer, dui amet magna, nisl dolor nonummy.",
    "Donec adipiscing urna suspendisse ultricies, nonummy tortor, turpis libero vel maecenas vestibulum porttitor, tempor integer, aliquam leo facilisis non vehicula. Commodo quisque consectetuer neque suspendisse rhoncus amet, nullam lorem mauris ut aliquip erat, enim ligula. Faucibus tincidunt donec consequat suscipit viverra nibh, etiam imperdiet gravida. Cum hac eu condimentum eu, consectetuer maxime dolor maecenas, rutrum dis litora ut. Venenatis aenean neque sed, nisl condimentum mus lorem accumsan, amet tincidunt ut libero in, rutrum vestibulum. Sit aliquam adipiscing sed erat, eleifend eu elit commodo pede nec, mauris et, vel nec cras. Non libero quis venenatis praesent, convallis nibh mauris est, pede lorem.",
    "Imperdiet nonummy montes dui justo massa in, fermentum est diam imperdiet nec libero lorem, lorem urna imperdiet maecenas phasellus, inceptos vestibulum, hendrerit ultricies nunc adipiscing quisque in eget. Sem mauris wisi nunc, porta vel ullamcorper wisi, ipsum senectus volutpat, pellentesque enim, ut vestibulum. Mauris quisque ridiculus, rhoncus sit bibendum. Ante montes, elit adipiscing urna ligula wisi tristique erat, tristique augue quos in mauris tellus neque, erat vivamus ipsum, justo pellentesque iaculis quam aptent sapien per. Rerum feugiat vulputate ante, tortor primis amet nulla, justo enim conubia id elementum duis et, eros nunc enim arcu id integer cursus. Vehicula tincidunt, a lorem netus luctus nulla, commodo quam ligula pellentesque. Rutrum dolor senectus erat enim, posuere porta consequat aenean, adipiscing id bibendum mollis vel sed feugiat, suspendisse ullamcorper vel sem magna a magna. Nulla et nulla leo, fermentum sollicitudin nulla. Elit omnis voluptas, iaculis sed sagittis mauris. Risus aenean potenti, ratione non eros. Gravida sed, condimentum turpis. Wisi donec vitae, sint varius habitant ut nulla.",
    "Nec amet sed porttitor viverra adipiscing, et mauris aut sem pretium, felis erat, et nunc risus. Fermentum hendrerit vestibulum arcu. Ultricies dui et dis netus, ipsum metus ut vestibulum lectus. Reprehenderit in augue nunc, quis nonummy curabitur nec non wisi, libero sed sed amet dignissim id placerat, aliquam est curabitur consectetur. Donec donec vestibulum mauris potenti tellus, pharetra venenatis elit. Senectus luctus vel congue nonummy, augue facilisis eros, odio odio imperdiet, ligula sollicitudin ante cras eros arcu, quis hendrerit. Placerat sodales, dolor interdum sem mi per, amet nisl sed sapien dolor, itaque luctus quisque donec, phasellus curabitur nonummy. Mollis suspendisse est suscipit rhoncus, per nonummy consectetuer nonummy, enim lobortis ultricies libero quis nulla aliquam, dui proin elit in. Ligula vitae rutrum.",
    "Convallis risus velit, sed elit cum sociis libero, accumsan praesent urna orci rutrum, quam nibh praesent odio purus blandit. Pellentesque gravida elit iaculis, felis proin est elit nibh, dictumst magna id purus maecenas placerat quisque, est non dapibus ante quisque, nam sit penatibus. Velit nascetur nonummy nam, orci maecenas aptent quisque, nibh penatibus wisi, ut sodales aliquam velit in natoque blandit. Dolor cursus magnis. Ac nulla adipiscing urna purus. Dui vestibulum magna tortor, posuere non vestibulum, consequat sem class nisl libero ac quam, sem ut risus congue bibendum consequat, leo aliquet neque odio dictumst tincidunt. Mollis dolor donec etiam, nulla nam mauris blandit lacinia sed etiam, tellus arcu litora nullam potenti, egestas bibendum. Varius condimentum vulputate turpis mi, non sem leo ornare quis, massa integer lobortis vitae. Hac nec donec amet, deleniti elit vestibulum ac arcu, at fringilla enim nam elit vel. Ut pede nonummy, erat taciti porta, nascetur nisl vivamus et ullamcorper, sit praesent nam tincidunt posuere sodales, vivamus eget tempor sollicitudin integer. Consectetuer vitae nec nibh metus mauris, lectus volutpat amet tincidunt lacinia, lorem ac pharetra enim eu, vestibulum id wisi fusce amet. Ipsum nonummy erat, sapien mus consectetuer dapibus.",
    "Vitae non maecenas amet, sapien non ante integer nunc sed, in curabitur. Donec in sit elit mauris sed, enim vel, nec pellentesque eleifend sit dui. Nulla suscipit diam magna mus cras, non nulla fringilla viverra a ligula, luctus quisque hac ultricies ut tincidunt, mauris vel, orci in imperdiet aliquam. Libero mauris suspendisse eget sem. Vitae nonummy pellentesque non nibh, sed porttitor scelerisque hendrerit vel aliquam, odio ultricies interdum ut sociis sit, ut tincidunt cras arcu, venenatis montes dolor. Tempor nulla et urna est, viverra risus habitant ornare. Sem vestibulum, ipsum enim leo hendrerit. Augue eleifend sed pellentesque faucibus nullam aliquam, aliquam mauris eget fusce morbi, arcu praesent viverra, elementum erat, malesuada adipiscing risus amet arcu tristique.",
    "Phasellus erat enim, cras duis ante. Vel varius etiam maecenas fringilla, ullamcorper auctor odio commodo suscipit. Risus id ac leo mollis arcu vel. Lacinia nullam, proin id faucibus at ac, rhoncus donec tincidunt. At mauris, libero praesent ornare ut urna, lobortis lacus et elit quam, convallis id felis eleifend nonummy taciti, felis libero gravida wisi justo quam. Diam turpis faucibus, feugiat sapien lacinia risus sed, dolor erat est eros, aliquam mauris mattis aliquet facilisis magna quam. Elit sed vulputate, tellus mauris nulla quam vitae mollis, ullamcorper adipiscing venenatis, accumsan mauris varius sem sed. A conubia fringilla, nunc pellentesque eget voluptatem quis vitae, maecenas vitae est faucibus vivamus diam, cursus ante nullam proin sem. Vel consequat sit metus vel lectus habitant, aenean aenean mauris facilisi faucibus porttitor quis. Eget quis.",
    "Suscipit vel at tristique mattis. Egestas sem mauris aliquam. Facilisis nisl non vel aptent tempor, aliquam feugiat nisl vitae eu, vel sed orci, porttitor nam massa eu quisque. Commodo nibh et justo risus pellentesque, nam faucibus. Eget nisi nisl, volutpat lectus duis, adipiscing at, cum pretium. Velit sed laoreet. Quam quis in nulla qui vivamus mauris, justo sodales ligula, leo blandit velit. Eaque porttitor et, ac est penatibus massa id in. Placerat arcu, tempor erat porta amet, erat odio in vestibulum in facilisi at.",
    "Dui a mauris vivamus curabitur ultricies, posuere maecenas id tincidunt interdum elementum viverra, turpis velit orci dolor libero pede, suspendisse at proin, nullam aliquam pede mauris donec aliquam. In congue volutpat quis wisi sollicitudin, dolor eget mauris ut lorem dignissim, vestibulum inceptos, ipsum sit, sit feugiat. Orci velit et cursus ultricies nulla. Curabitur in, orci adipiscing, lorem suspendisse, at nulla ultricies pede consectetuer arcu dolor. Nibh nunc in. Habitasse donec taciti accumsan est aliquet, ante est fermentum. Quis iaculis nullam odio consequat, suspendisse ex. Aliquam vel egestas in, pharetra ante repudiandae vulputate felis, nonummy rem rhoncus nunc lectus amet laoreet, cursus iaculis faucibus, pellentesque a non in wisi. Nullam dolor pede, congue libero integer porttitor a ut at, eget integer. Amet mollit nec, sed nulla dignissim dolor unde massa, elit natoque hendrerit viverra nec posuere, purus ipsum sed, ullamcorper pretium ipsum pharetra litora. Tellus velit viverra nonummy ornare, dictum netus diam consectetuer in, omnis mauris mauris lacinia nec fusce.",
    "Eget dui luctus ante, dapibus pellentesque sed luctus placerat. Mauris nunc fames. Molestie in eros pulvinar ullamcorper interdum tortor, pellentesque vel lobortis arcu faucibus sunt, mollis wisi consectetuer et quam nulla tellus. Sit ut viverra cras augue, ut lacus dolor, sollicitudin enim nunc turpis. Et vestibulum viverra lacus malesuada posuere. Sed suspendisse placerat accumsan varius praesent, aliquam lacus, nibh lacus justo sed erat, proin ultrices. Ligula magnis tristique, mus urna nulla nec lacus elit a, ac commodo sed, pulvinar potenti. Sagittis elit at aliquet pellentesque mauris sociosqu, montes imperdiet mollis, eu amet vitae consectetuer. Lobortis quia bibendum ac massa imperdiet vel, sit viverra eget, nulla sem, blandit nec.",
    "Fermentum ut sit faucibus aliquam, mauris venenatis, enim rutrum rhoncus vitae nec, nibh commodo lorem ultricies lobortis augue duis. Iaculis lectus, in fusce, aliquam magna mauris eu enim eu. Cursus morbi metus vel porttitor ut, nulla tellus, in ullamcorper mauris auctor ante quisque erat. Sapien non vestibulum turpis, commodo egestas enim viverra at erat sit. Mauris tortor platea. Nam pharetra diam taciti, vivamus tempor vitae. Elit id, tincidunt leo cursus pulvinar nulla, natus sagittis nullam, aliquam feugiat, mattis non nec sem augue. Urna ullamcorper, massa magna, et fringilla metus lacus, tempus bibendum ridiculus. Excepturi interdum.",
    "Et odio dolor, nulla ultricies tellus turpis, ultricies commodo purus augue quisque, semper ac non. Aenean dis ipsum, magna est dictumst. Nec mollis ut, rutrum wisi fringilla facilisis magna, massa dolor sagittis quis iaculis. Tortor odio vestibulum vel velit, ante fermentum fusce augue, qui morbi praesent, viverra consequat pede. Dolor felis a nunc, elit augue donec dis, pharetra at phasellus leo nam phasellus, neque et sit potenti sit urna.",
    "Vel at, amet wisi ridiculus, dolor pellentesque urna eu tortor, fusce tincidunt tellus, proin leo et placerat donec enim. Sit congue malesuada, condimentum pellentesque. Ac suspendisse dictumst elit egestas. Integer placerat in, non etiam ut phasellus vitae aenean quam, mauris metus justo mauris elementum arcu quis, eros amet velit ut, dictum amet. Mi eget ad laoreet eu. Ligula massa, leo nulla congue, eu morbi fusce viverra non at, id dolor mauris.",
    "Vitae eros mattis in, molestie placerat imperdiet felis. Vehicula diam conubia aliquam adipiscing ut pretium. Justo elit arcu varius wisi nec massa, lectus mauris cras neque. Aenean urna vestibulum nonummy, ipsum non iaculis mauris gravida, tristique dignissim malesuada faucibus morbi, ullamcorper nullam vivamus mollis, urna porta id. Donec id in amet neque neque accumsan, magnis arcu risus in, ultricies vitae mollis convallis, semper eget ut nulla enim eros quis, vestibulum ut harum. Magnis id sit sapien congue elit nec, fusce ut a etiam molestie penatibus, mauris sit. Pellentesque sed et nonummy pede nonummy suspendisse, est curabitur, aspernatur massa lectus morbi. Erat sed vel nunc, in consectetuer, feugiat mauris quam habitant. Tortor quis non ipsum rutrum id bibendum. Nullam hac quam lobortis pede, ultrices pharetra neque aenean, amet inceptos donec nulla interdum.",
    "Pharetra mauris nonummy, pharetra eget erat feugiat eu mi, pellentesque nunc risus, aliquam minim velit fringilla, justo lorem enim etiam lorem. Nulla wisi tincidunt adipiscing mi amet, phasellus augue aliquam id tellus metus, augue donec. Accumsan duis enim nunc ridiculus. Sit massa lorem, et integer sapien. Non ac, eget turpis suscipit, eu qui sem commodo aliquam, ac at nam non a ad.",
    "Nunc adipiscing arcu vivamus eros. Lobortis risus, dictum quis lectus accumsan nunc amet natus, penatibus amet blandit enim et vitae, at sed ut rutrum vestibulum euismod a, vel lectus diam vel et augue. Parturient dis mauris mollis, pulvinar tellus luctus pede, in aliquet vestibulum eu, conubia enim, justo cras massa suspendisse. Nibh lacus erat elit, faucibus aliquet ut, pede sapien ullamcorper, sodales sem enim lobortis tincidunt. Felis non enim fusce, donec magna morbi, vestibulum rutrum nec lorem, nullam urna nullam eu hendrerit phasellus, nec nunc ultricies nibh consequat faucibus. In vehicula, cras adipiscing ornare dolor adipiscing perferendis bibendum, nonummy est, vivamus porta massa. Purus lorem nonummy pede fusce molestie, facilisis porta mauris euismod tincidunt interdum nisl, blandit turpis commodo dapibus ac, aliquam ultrices auctor lorem consequatur, pellentesque tincidunt cras. Neque arcu nisl sed. Pulvinar libero leo vero libero, orci amet lacus felis venenatis vestibulum, mauris tincidunt, sit scelerisque varius. Neque dis. Mattis non morbi nulla, est nullam sed pellentesque cras. Enim erat.",
    "Mi donec, eget eget, imperdiet aliquet volutpat adipiscing ante. Et ut urna quam lacus facilisis, accumsan auctor tristique. Mauris duis arcu. Proin ullamcorper. Eget felis, tincidunt nibh morbi mauris sodales leo. At at condimentum.",
    "Eu id nulla ipsum. Proin sit diam proin, justo tempor enim pede pellentesque vulputate gravida. Nulla augue, ante eu vitae vivamus. Vitae et id, donec et. Sed vel. Vestibulum id et metus quam ullamcorper wisi, nisl lectus risus ut. Sapien donec risus, sit orci aliquet risus hymenaeos amet consequat, scelerisque dolor pellentesque.",
    "Arcu proin, interdum arcu wisi tellus purus placerat commodo, suspendisse molestie sit, laoreet placerat montes suspendisse vel. Id sapien a, lobortis per, imperdiet blandit, neque quis maecenas malesuada malesuada. Lectus pede eu vehicula per aenean. Integer per ante viverra, maecenas dapibus odio felis aliquam ullamcorper, massa nostra condimentum convallis maecenas eget porttitor, per cubilia, velit corrupti vivamus turpis. Nunc enim nunc, augue risus, ultricies tristique et non arcu, eu placerat. Bibendum massa, aliquam metus faucibus per natoque ultricies egestas, orci primis tellus, vestibulum id. Nunc semper velit.",
    "Libero wisi lorem leo sit, vivamus nunc facilisis nunc amet ut urna, eget voluptatibus metus nunc massa imperdiet, commodo mauris, augue proin blandit suscipit suspendisse et erat. Wisi sed nascetur elit soluta. Odio amet nec maecenas, cursus sodales eros at amet felis, vel maecenas sollicitudin vestibulum donec nec arcu, et at lacus rutrum, maecenas eros. Dolor netus cursus nisl. Curabitur vestibulum blandit feugiat nec elit eleifend. Sem magna arcu placerat porttitor nec, quam ultricies erat tellus turpis eget. Quisque wisi curabitur neque rutrum, elementum et eget nunc varius nulla. Curabitur volutpat montes urna sit pharetra dolor, pede ultrices dignissim fusce duis accumsan fringilla, eget fermentum suscipit, vel elementum, tenetur consequat dui congue lacus ullamcorper. Elit elit diam elementum, ipsum condimentum hymenaeos.",
    "Nonummy class aliquam lacus placerat, id in a in tellus natoque semper, quis maecenas, at nulla, et nunc proin. Sem sit orci. Aliquam turpis et aliquam, ultrices eu augue. Sem posuere, tincidunt donec sapien. Curabitur lacus elit arcu, congue magna non neque ridiculus sit eget, molestie duis, sit sem eros in a arcu nunc. Sem sit id vel cursus, pulvinar orci suspendisse suspendisse magna ultricies feugiat, in sit, in odio viverra nec eros voluptatibus neque. Mauris porttitor commodo malesuada vel sodales magna. Ab orci purus lobortis ligula orci rhoncus, amet amet sed a, dui feugiat dapibus ante dolor vestibulum quam, maecenas felis tortor et nonummy, mauris quisque sed varius nibh aliquam sociosqu.",
    "Consectetuer wisi qui a enim diam morbi, phasellus wisi ut sed a sem luctus, integer libero in faucibus aliquam adipiscing nibh, eu et commodo etiam maecenas ligula vestibulum. Mauris interdum ullamcorper consectetuer, et montes arcu risus nec velit id, consectetuer wisi sit phasellus risus dictum, cras exercitation accumsan enim, facilisis nam quis odio. Blandit iaculis sem elit nulla, turpis neque mauris laoreet, at eget molestie vestibulum, pulvinar arcu adipiscing in at ultrices mollis. Commodo congue lacus nulla. Ut lectus maecenas provident per mi vel, ipsum faucibus pede aliquam lacus, id massa, suspendisse faucibus et pulvinar. Molestie nunc ornare eu, id lacinia mauris. Augue cursus consectetuer id metus, in justo sed wisi urna, tortor lacus interdum pede volutpat vel etiam. Amet pharetra, vel sed eget lorem nulla sed, bibendum lorem, lectus est aliquam vestibulum feugiat lorem ante. Penatibus vehicula enim augue massa. Nunc voluptas blandit interdum lacinia gravida cras, montes faucibus, odio mauris suscipit felis. Lobortis tempor id vel mauris feugiat eget, lectus donec eu vitae, sed molestie turpis. Tortor et convallis elementum malesuada arcu, erat imperdiet turpis, felis rhoncus ac vitae sit, mauris consequat sit tortor maecenas.",
    "Faucibus lacus, wisi odio est cras, mi vestibulum et elit sed, nec sed nec est integer, porttitor vitae ullamcorper. At duis vulputate vulputate dui nulla lorem, odio non, tincidunt faucibus purus ut, varius vestibulum arcu. Vel integer tortor imperdiet nullam tempus. Id ligula, dolor wisi eleifend, nullam vitae erat faucibus vulputate, tristique ipsum blandit vulputate. Nam mauris tellus elit pellentesque mi, tincidunt turpis proin sollicitudin fames varius, viverra in, ridiculus tortor risus sit. Purus lacus tempus vestibulum donec, eu libero eget tincidunt molestie leo, mauris diam ipsum, ipsum conubia massa felis molestie. Luctus iaculis, sit erat venenatis vehicula velit dolor. Donec cras proin tellus lacus facilisis amet, convallis ligula, imperdiet nam vestibulum pellentesque hic ac. Mauris vitae mauris urna faucibus odio lacus, sed maecenas molestie in beatae varius metus, et ultricies eget nec omnis consequat.",
    "Massa tenetur mollis malesuada accumsan etiam vitae, eleifend fringilla donec interdum, id mi sapien in, vulputate tortor mauris integer quo erat. Et leo turpis numquam massa neque, nec sit ultricies consequatur erat, curabitur consectetuer pellentesque aliquam placerat, elit enim neque sit dolor suscipit. Gravida dui donec vitae accumsan nec. Lectus laoreet. Quam metus aptent lobortis morbi natus pellentesque. Duis sollicitudin, nullam fermentum sagittis quis mollis at, fermentum cras, a egestas. Rutrum volutpat laoreet, ut donec felis tortor dui luctus, esse in cras morbi felis enim velit, suspendisse malesuada nec, viverra molestie quam blandit. Lacus sed pellentesque sit vel quis tortor, quia sed netus mauris, cum id pellentesque, nec elit luctus sem. In sollicitudin lobortis sed amet ornare dapibus, sapien faucibus vestibulum, ante tellus eget eros. Non dignissim sem non libero, nihil donec porttitor, placerat metus. Eu est, odio leo mi praesent ante lectus dictum, nam accumsan sodales justo, urna suspendisse molestie ante fusce, ac eros fusce. Facilisis sollicitudin, magna diam mauris purus turpis integer orci, in curabitur, magna luctus metus.",
    "Neque neque quam, sed cras ligula vitae, diam vulputate nibh ultricies, hac pellentesque dui erat ultricies elementum tortor, tellus rutrum scelerisque dignissim vitae et. Enim libero, metus cras, ac sint egestas asperiores, tortor varius aenean vestibulum, purus ante arcu aliquam consequat a nec. Senectus at lacus sed diam, vitae et, massa etiam lectus, ut sed, vehicula maecenas nec suspendisse mattis. Nec amet ut tellus, placerat enim viverra, cum elit. Nulla fringilla, sed eget praesent laoreet nonummy velit. Tortor et quis pellentesque, dolor proin arcu in mauris eu eros, convallis lorem ut id. Justo donec suspendisse elit lectus praesent sit, adipiscing tellus quis ac non, condimentum integer, non dolor, odio rutrum nec odio dictumst metus. Diam tellus, tortor a duis vehicula, et hymenaeos curabitur ut purus, pretium quis, odio in praesent felis ipsum. Odio nulla pretium volutpat adipiscing imperdiet.",
    "Id odio, in wisi sapien id convallis proin, aliquam commodi placerat maecenas nam vitae morbi, sit venenatis sapien ante et ante aliquet, neque sed adipiscing inceptos urna vehicula arcu. Justo sed vel. Nulla nulla rutrum per posuere ut, ultricies varius at. At aliquam sem eu. Lacus tristique euismod. Tellus augue vulputate habitasse lorem vel. Wisi turpis pede et taciti sapien, luctus neque vestibulum sociis nibh massa, nulla mi consectetuer eget duis erat. Elementum montes orci, metus consectetuer, amet iaculis turpis ullamcorper a velit donec. Non nunc, elit feugiat, tellus vulputate enim sit accumsan. Vestibulum erat ante nec scelerisque elit malesuada, eget suspendisse interdum mauris, sit felis pede, dui tincidunt eget quis, sit montes suspendisse.",
    "Eget elit eleifend nulla ultricies ultricies a, posuere luctus nec hac in, neque urna rerum commodo felis. Ut nullam suspendisse blandit lacinia a. Blandit sagittis molestie, ac pellentesque aptent quisque quam neque sed. Justo accumsan lorem amet lacus est ac, vulputate vehicula nullam malesuada, ligula porta ante, ut pulvinar vestibulum aliquam non. Labore semper est etiam elementum at, nam rhoncus, urna lectus. A accumsan justo leo.",
    "Nam tempus, malesuada faucibus facilisis posuere, lectus integer quia sagittis, enim amet integer orci, senectus non sem erat amet enim vel. Risus urna, cursus diam sit, suscipit quam eros at posuere, laudantium lacus. Lectus eu proin eleifend, dolor sapien risus id ac nullam at, netus odio morbi integer at, ut lorem donec justo, aenean arcu integer. Vestibulum viverra sagittis, risus nostra diam sodales, eget dictum condimentum vestibulum proin, ad adipiscing nulla vel elit anim, gravida lectus ligula volutpat scelerisque fusce. Quis sed sem vestibulum penatibus, mi donec, et vivamus vestibulum, vivamus ultricies magnis morbi convallis nam nam, fermentum sit etiam omnis volutpat. Curabitur mauris ultrices mauris pulvinar porttitor, quisque integer tellus, netus vel. Vel etiam ipsum felis dapibus nulla eu, suspendisse quis in etiam lacus, amet ut congue senectus habitant nunc, orci diam morbi aliquam. Fusce eu elit proin odio amet.",
    "Fusce mauris augue mi cursus hymenaeos quam, ante volutpat nulla mattis. Aliquam dolor velit, erat sed. Mi dolor curabitur vitae amet, reiciendis elementum pellentesque, ipsum facilisi in non morbi cursus morbi. Morbi id, per turpis cras, in est velit, ante urna viverra tristique lectus pellentesque. Justo felis purus morbi, fames habitasse tellus ante, vivamus elementum in. Nunc donec tincidunt velit tortor, et ac, varius ligula egestas suscipit. Pulvinar pellentesque dolor mauris ut, nec libero scelerisque tellus dignissim purus, maecenas vel montes duis, justo quis aliquet dolor odio ullamcorper mollis. Elit amet justo lacus metus ac, sodales etiam, amet vulputate ridiculus blandit aliquam, et erat ut dui maecenas.",
    "Condimentum felis aliquet magna nec tellus, nam nonummy sit nam amet, arcu vel id vestibulum mollis dui suspendisse, non a. Cubilia porta imperdiet blandit euismod est, quam tincidunt, eu id etiam penatibus quam pretium in, metus tempus pulvinar placerat velit. Mi et in suscipit quis sed, metus bibendum sodales. Nibh turpis, vitae augue tellus sit ante, dapibus in tellus libero nullam morbi. Ornare pellentesque penatibus ornare volutpat erat, phasellus quam justo odio amet ullamcorper. Vestibulum nec et aenean ipsum ipsum, erat dolor ligula eget aptent, dignissim augue lorem, sed mauris mi lacus arcu quisque felis, leo nullam dignissim similique ipsum. Bibendum quisque sed lectus posuere parturient.",
    "Fringilla fringilla, amet nonummy curabitur donec, arcu in felis urna morbi, tincidunt mi eu amet vehicula porta duis. Donec vitae varius sed pellentesque ultrices nec, inceptos nibh libero. Quis wisi massa pellentesque donec, mollis ut volutpat. In libero praesent fringilla, sit facilisis cursus nibh quis semper. Vestibulum elit, ultricies in, luctus condimentum imperdiet deleniti arcu. Ligula nisl libero elit, feugiat dolor ullamcorper felis vel mauris adipiscing, amet diam purus, ultrices tincidunt lacus id leo, fames mollis congue rutrum consectetuer massa aliquet. Ullamcorper libero vehicula volutpat, arcu eu placerat, donec pellentesque. Semper ultricies euismod ornare senectus, nibh neque omnis id sit, quam convallis. Nunc ultrices cras vel tempor sem ac, gravida platea molestie amet gravida tortor quis. Turpis ac libero.",
    "Adipiscing pede dolor in donec ipsum excepteur, nunc aliquam eu dui arcu, tempor eum sed ut, id pede odio ut ridiculus in, cursus dolor in pede. Urna tempor eleifend, sollicitudin nulla torquent sodales amet nam et, pede ultrices est id diam. Nulla massa consectetuer at nascetur do, in euismod porttitor. In turpis, amet elit quis, id ut, et non, sagittis ipsum mollis aliquam. In imperdiet in, sodales fermentum in feugiat eu, nonummy sed feugiat. Nunc mauris a neque sed venenatis. Elit vestibulum, ipsum mauris sit phasellus. Autem rhoncus class duis sed sem integer, praesent vel, ligula et lectus wisi maecenas aenean adipiscing, arcu pede cubilia pellentesque at elit aliquam, ante diam velit sociis augue sem ligula. In lobortis volutpat fusce pellentesque, aliquam augue suspendisse ac, reprehenderit et pulvinar fringilla aliquam mollis nunc. Dis vitae integer eget lorem. Phasellus dui aliquet risus nec convallis, penatibus cras sit amet amet, nunc felis rutrum porttitor diamlorem nulla, quisque sociosqu velit mauris.",
    "Mi tempor. Enim magna et quisque, nunc dis volutpat nonummy. Etiam faucibus erat est et, id nunc at posuere vestibulum, elit natoque lectus eu, sed aliquam. Turpis amet ut quis, sed sit cubilia tellus eu mattis. Cursus libero lorem. Nonummy optio in, libero sapien, pulvinar tempus. Interdum in quis non dapibus, vestibulum libero eu lectus est quisque in, odio tristique vivamus pharetra in enim. Quis fusce, sapien suspendisse et in commodo repellendus vel, et pulvinar, eleifend nunc cras vitae vivamus, natoque diam etiam vehicula sollicitudin arcu.",
    "Vitae et nulla, pede et nam, tortor magna id proin dictumst malesuada quis, diam aliquam mauris. Elementum leo pellentesque quis fusce morbi eu, at fermentum ac adipiscing malesuada ipsum. Ac placerat nec neque turpis interdum, morbi vitae ligula a venenatis pharetra, neque ut duis curabitur morbi auctor pede. Elementum enim sociis potenti, dui vivamus tellus tempus. Nam molestie. Congue lorem in adipiscing tellus commodo, at mauris nunc sit lacinia, posuere nunc faucibus varius in eget. Et neque sagittis cursus in torquent quis, aliquam hymenaeos odio nunc vel diam. Pulvinar ligula erat etiam dolor, aenean egestas, pede dui amet egestas, etiam porta. Ut nunc quis, pulvinar quis massa quisquam aenean molestie, libero placerat aenean dolor odio dolor, vel lorem, nunc potenti donec. In morbi in etiam nunc. Nam commodo nulla viverra, at magnis nunc tortor est non nulla.",
    "Morbi libero praesent vitae, orci curabitur wisi sed ut. Risus fusce vulputate libero. Nibh pulvinar veritatis etiam commodo vel rutrum. Malesuada urna vehicula, lectus ipsum pellentesque, enim augue nulla nec faucibus, suscipit facilisis iaculis tempor ut urna, at sit. Scelerisque at, erat non et mauris numquam viverra amet, in elit nam a vehicula scelerisque, commodo id mollis primis odio in libero. Sit porttitor arcu arcu, nam donec vestibulum aenean iaculis faucibus ullamcorper, dolor pretium. Pellentesque quis nonummy. Pretium nostrud lorem tortor sapien aliquam, felis nonummy vestibulum facilisis sodales, mauris accusamus, venenatis ac dui.",
    "Ultrices lobortis sed malesuada ipsum sed, congue sociosqu. Lobortis neque facilisis integer. Odio vel feugiat, donec mi nunc neque elit leo, cras ligula curabitur, sapien integer ad sed ipsum. Quis donec ut diam libero rutrum morbi, libero maecenas diam imperdiet fermentum purus, nunc tristique platea sollicitudin lacinia. Eros libero risus orci. Diam enim, in imperdiet in metus fermentum urna urna. Phasellus tristique consectetuer praesent id nulla, mauris wisi nonummy, egestas eget phasellus nulla, ornare non nec vel ut neque non.",
    "Et nobis mi pede ligula. Mus dolor ornare. Convallis fusce proin eu augue adipiscing nulla. Consectetuer vestibulum voluptatem fringilla id, tristique et aenean. Lacinia egestas ullamcorper non, luctus pede bibendum. Porttitor et urna rhoncus nec, dui leo urna dui bibendum. Praesent suscipit, rhoncus aliquam ut nec nec, senectus curabitur suscipit ut eleifend vestibulum. Semper suspendisse, integer in quibusdam gravida aliquam malesuada a, porta cum maecenas cras auctor suspendisse commodo. Vitae faucibus in purus, nullam adipiscing eget nullam a ullam. Quis sodales elementum tincidunt magna, libero dis amet libero ligula. Est donec vestibulum neque nascetur amet, eros scelerisque tellus consequat in ullamcorper nam, eu in wisi consequat pede. Quis amet urna ipsum, amet feugiat, ligula dignissim, porta elit sollicitudin augue arcu, pellentesque morbi elit nascetur.",
    "Vivamus volutpat volutpat nec elit commodo, mauris suspendisse nullam dis auctor et, aliqua eu vel ipsum sem augue, sodales scelerisque. Wisi vel odio diam non, ac varius, id vitae, luctus elit duis at suspendisse velit, duis aliquam netus phasellus quisque donec. Eu ut magna scelerisque diam elementum donec, luctus massa, nec erat arcu porttitor viverra libero magna, natoque metus enim neque, mauris nisl magna. Non vel id leo pede lectus. Pede eget sit dictum odio. Amet nullam mi risus praesent magna, quis sit laoreet dolor elit wisi. Wisi nam nunc, amet mollis, urna vestibulum sit magna etiam etiam. Vitae neque, pharetra consectetuer praesent, nec rhoncus suspendisse exercitation reiciendis eget venenatis, sit dicta sed id. Fringilla dolor wisi morbi vehicula, ultricies porta imperdiet cupiditate justo pellentesque pellentesque. Nibh egestas nam eros turpis ultrices lacus, nibh sociosqu, amet wisi, lectus dolor, et ante et. Diam mollis nunc commodo. Morbi arcu lorem lacus vitae dapibus, quis integer neque accumsan eros, adipiscing rutrum laborum tempus, proin cum justo morbi, neque quis parturient nulla elit magna et.",
    "Quam id proin suscipit leo libero, pharetra consectetuer elit. Maiores vitae. Nec vel, faucibus adipiscing porttitor consequat, nullam etiam sed libero sem amet, tincidunt ut leo curabitur ridiculus et. Mollis eget non arcu libero, venenatis eget diam, ac integer dapibus praesent accumsan quam, nulla nonummy mi vel arcu sed, quam sed. Condimentum proin in risus, vivamus vivamus donec, mauris vulputate risus, consectetuer hymenaeos donec, tellus at aliquid consectetuer. Quam dui conubia sit porttitor, cursus euismod, a orci leo. Arcu sed lacinia, vestibulum mi pellentesque amet parturient nibh neque. Integer suspendisse mollis et sed nam posuere, cras eros id non, posuere pellentesque.",
]


/*companyNames.forEach(companyname => {
    bcrypt.hash(companyname, 10, function(err, hash) {
        if (err)
        {

        }
        else
        {
            let newUser = new userModel();
            newUser.username = companyname;
            newUser.password = hash;
            newUser.email = companyname + "@" + companyname + ".dk";
            newUser.companyname = companyname + " " + companytypes[Math.floor(Math.random() * (3 - 0))]
            newUser.save();
        }
    });
})*/

let id = 1;

for (let cat = 0; cat < 10; cat++)
{
    let random = Math.floor(Math.random() * (13));
    console.log(random)
    for (let reg = 0; reg < 13; reg++)
    {
        if (reg != random)
        {
            let randomJobCount = Math.floor(Math.random() * (50 - 35)) + 35;
            for (let job = 0; job < randomJobCount; job++)
            {
                let newJob = new jobModel();
                let randomTitle = jobtitles[Math.floor(Math.random() * (jobtitles.length - 1))];
                let text = "";
                let randomparagraphs = Math.floor(Math.random() * (6 - 3)) + 3;
                for (let p = 0; p < randomparagraphs; p++)
                {
                    if (text.length > 0) text += "\n\n";

                    text += texts[Math.floor(Math.random() * (texts.length - 1))]
                }


                    let month = Math.floor(Math.random() * (6 - 5)) + 5;
                    let day = 1;
                    if (month === 5)
                    {
                        day = Math.floor(Math.random() * (30 - 15)) + 15;
                    }
                    else
                    {
                        day = Math.floor(Math.random() * (7 - 1)) + 1;
                    }

                    newJob.date = "2019" + "-" + (("0") +  month).slice(-2) + "-" + (("0") +  day).slice(-2);
                    newJob.title = randomTitle;
                    newJob.text = text;
                    newJob.category = categories[cat];
                    newJob.region = regions[reg];
                    newJob.user = companyNames[Math.floor(Math.random() * (companyNames.length - 1))]
                    newJob.id = id;
                    newJob.save();
                    id++;
            }
        }
    }
}

