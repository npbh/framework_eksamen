module.exports = (mongoose) => {
    const express = require('express');
    const router = express.Router();
    const bcrypt = require('bcrypt');
    const jwt = require('jsonwebtoken');

    //bruger skema
    let categorySchema = mongoose.Schema({
        id: String,
        name: String,
        count: Number
    });

    let regionSchema = mongoose.Schema({
        id: String,
        name: String,
        count: Number
    });

    let jobSchema = mongoose.Schema({
        id: Number,
        title: String,
        text: String,
        category: String,
        region: String,
        user: String,
        date: String
    })

    let regionModel = mongoose.model('region', regionSchema, "regions");
    let categoryModel = mongoose.model('category', categorySchema, "categories");
    let jobModel = mongoose.model('job', jobSchema, "jobs");

    router.get('/regions', (req,res) => {
        console.log("regions")
        regionModel.find({}, (err, result) => {
            if (err) return console.log(err);
            if (result)
            {
                res.json(result);
            }
            else
            {
                res.json({msg: "no regions found"})
            }
        });
    });

    router.get('/regions/:cat', (req,res) => {
        regionModel.find({}, (err, result) => {
            if (err) return console.log(err);
            if (result)
            {
                let regionCount = result.length;
                result.forEach(region => {
                    jobModel.find({category: req.params.cat, region: region.id}).countDocuments((err, amount) => {
                        region.count = amount;
                        callback()
                    })
                })

                function callback()
                {
                    regionCount--;
                    if (regionCount === 0)
                    {
                        res.json(result);
                    }
                }
            }
            else
            {
                res.json({msg: "no regions found"})
            }
        });
    });

    router.get('/categories', (req,res) => {
        categoryModel.find({}, (err, result) => {
            if (err) return console.log(err);
            let categories = [];
            if (result)
            {
                let catCount = result.length;
                result.forEach(category => {
                    let cat = category.id;
                    jobModel.find({category: cat}).countDocuments((err,amount) => {
                        categories.push({id: category.id, name: category.name, count: amount})
                        callback();
                    })
                })

                function callback()
                {
                    catCount--;
                    if (catCount === 0)
                    {
                        res.json(categories);
                    }
                }


            }
            else
            {
                res.json({msg: "no categories found"})
            }
        });
    });

    router.get('/job/:id', (req,res) => {
        jobModel.findOne({id: req.params.id}, (err, result) => {
            if (err) return console.log(err);
            if (result)
            {
                res.json(result);
            }
            else
            {
                res.json({msg: "no jobs found"})
            }
        });
    });

    router.get('/', (req,res) => {
        jobModel.find({user: req.user.username}, (err, result) => {
            if (err) return console.log(err);
            if (result)
            {
                res.json(result);
            }
            else
            {
                res.json({msg: "no jobs found"})
            }
        });
    });

    router.post('/job/', (req,res) => {

        jobModel.find({}, {id: 1}).sort({id: -1}).limit(1).exec().then(result => {
            let id = 1;
            if (result.length > 0)
            {
                id = result[0].id
                ++id;
            }
            let job = new jobModel();
            job.id = id;
            job.region = req.body.region;
            job.category = req.body.category;
            job.title = req.body.title;
            job.text = req.body.text;
            job.user = req.user.username;
            let date = new Date();
            job.date = date.getFullYear() + "-" + ("0" +(date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
            job.save();
            res.json({msg: "new job posting saved", id: id})
        })
    });

    router.get('/:cat/:reg/:page', (req,res) => {
        let category = req.params.cat;
        let region = req.params.reg;
        let pagesize = 10;
        let skip = req.params.page === "1" ? 0:pagesize*(Number(req.params.page) -1);
        console.log(req.params.page)
        jobModel.find({category: category, region: region}).countDocuments((err, count) => {
            jobModel.find({category: category, region: region}).skip(skip).limit(pagesize).exec((err, result) => {
                if (err) return console.log(err);
                if (result)
                {
                    res.json({pages: Math.ceil(count/pagesize), jobs: result});
                }
                else
                {
                    res.json({msg: "no jobs found"})
                }
            });
        })
    });

    return router;
}